#ifndef CLOCK_H
#define CLOCK_H


//-------------------------- Libraries-------------------

#include <semaphore.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "si_ui.h"



// ----------------------- Data Structures------------------------

/*Time data type */
typedef struct
{
	int hours;
	int minutes;
	int seconds;
} time_data_type;

/* clock data type */
typedef struct
{
	// the current time
	time_data_type time;
	// alarm time
	time_data_type alarm_time;
	// alarm enabled
	int alarm_enabled;
	
	// semaphore for mutal exclusion
	sem_t mutex;
	
	// semaphore for alarm activation
	sem_t start_alarm;
	
	// semaphore for display synchronisation
	sem_t show_alarm;
	
} clock_data_type;



//---------------------- Variables------------------------------------

// Create the acctual clock!
static clock_data_type Clock;


// create tasks
	pthread_t clock_thread_handle;
	pthread_t com_thread_handle;
	pthread_t alarm_thread_handle;


//---------------------------Functions---------------------------

//............General...............................

// Check if the time input in a set or alarm message from UI is allowed.
int time_ok(int hours, int minutes, int seconds);

// Compare a time object to the current time
int compare_to_curr_time(int hours, int minutes, int seconds);

//............Clock.................................

/* clock_init: initialise clock */ 
void clock_init(void); 

/* clock_set_time: set current time to hours, minutes and seconds */ 
void clock_set_time(int hours, int minutes, int seconds); 

// count the clock
void increment_time(void);

// Get current time
void get_time(int *hours, int *minutes, int *seconds);

// Extract the time from a set message from UI
void time_from_set_message(char message[], int *hours, int *minutes, int *seconds);



//.............Alarm.....................

// Set the alarm time
void alarm_set_time(int hours, int minutes, int seconds);

// Get current alarm time
void get_alarm_time(int *hours, int *minutes, int *seconds);

// Extract the time from an alarm message from UI
void time_from_alarm_message(char message[], int *hours, int *minutes, int *seconds);

// get value of Clock.alarm_enabled, protected
int alarm_enabled(void);

//.............Communication with UI..........
//  Uses mostly functions in display.h and string and std libs

//..............Threads....................

// The clock thread
void *clock_thread(void *unused);

// The alarm thread
void *alarm_thread(void *unused);

// Communication thread
void *com_thread(void *unused);


#endif

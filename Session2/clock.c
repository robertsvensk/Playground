#include "clock.h"

//.......................General...........

/* time_ok: returns nonzero if hours, minutes and seconds represents a valid time */ 
int time_ok(int hours, int minutes, int seconds)
{
    return hours >= 1 && hours <= 12 && minutes >= 0 && minutes <= 59 && 
        seconds >= 0 && seconds <= 59; 
}

int compare_to_curr_time(int hours, int minutes, int seconds)
{
	int curr_h, curr_m, curr_s;
	get_alarm_time(&curr_h, &curr_m, &curr_s);
	
	return hours == curr_h && minutes == curr_m && seconds == curr_s;
}

//.......................Clock.......................

/* clock_init: initialise clock */ 
void clock_init(void)
{
    /* initialise time and alarm time */ 

    Clock.time.hours = 12;
    Clock.time.minutes = 0;
    Clock.time.seconds = 0;

    Clock.alarm_time.hours = 0;
    Clock.alarm_time.minutes = 0;
    Clock.alarm_time.seconds = 0;
    
    /* alarm is not enabled */ 
    Clock.alarm_enabled = 0;
	//Clock.alarm_active = 0;

    /* initialise semaphores */ 
    sem_init(&Clock.mutex,0, 1);
    sem_init(&Clock.start_alarm,0, 0);
}

/* clock_set_time: set current time to hours, minutes and seconds */ 
void clock_set_time(int hours, int minutes, int seconds)
{
    sem_wait(&Clock.mutex);
	
    Clock.time.hours = hours;
    Clock.time.minutes = minutes;
    Clock.time.seconds = seconds;

    sem_post(&Clock.mutex);
}

/* increment_time: increments the current time with 
   one second */
void increment_time(void)
{
    /* reserve clock variables */ 
    sem_wait(&Clock.mutex);

    /* increment time */ 
    Clock.time.seconds++;
    if (Clock.time.seconds > 59)
    {
        Clock.time.seconds = 0;
        Clock.time.minutes++;
        if (Clock.time.minutes > 59)
        {
            Clock.time.minutes = 0;
            Clock.time.hours++;
            if (Clock.time.hours > 12)
            {
                Clock.time.hours = 1; 
            }
        }
    }

    /* release clock variables */ 
    sem_post(&Clock.mutex);
}

/* get_time: read time from common clock variables */ 
void get_time(int *hours, int *minutes, int *seconds)
{
    /* reserve clock variables */ 
    sem_wait(&Clock.mutex);

    /* read values */ 
    *hours = Clock.time.hours; 
    *minutes = Clock.time.minutes; 
    *seconds = Clock.time.seconds;
        
    /* release clock variables */ 
    sem_post(&Clock.mutex);
}

/* time_from_set_message: extract time from set message from user interface */ 
void time_from_set_message(char message[], int *hours, int *minutes, int *seconds)
{
    sscanf(message,"set:%d:%d:%d",hours, minutes, seconds); 
}

//............................Alarm...............

// alarm_set_time: set the time of the alarm and enable it.
void alarm_set_time(int hours, int minutes, int seconds)
{
	sem_wait(&Clock.mutex);
	
	// set alarm time
	Clock.alarm_time.hours = hours;
	Clock.alarm_time.minutes = minutes;
	Clock.alarm_time.seconds = seconds;
	
	//enable the alarm
	Clock.alarm_enabled = 1;
	
	
	sem_post(&Clock.mutex);
	
	display_alarm_time(hours, minutes, seconds);
}

/* get_alarm_time: read alarm time from common clock variables */ 
void get_alarm_time(int *hours, int *minutes, int *seconds)
{
    /* reserve clock variables */ 
    sem_wait(&Clock.mutex);

    /* read values */ 
    *hours = Clock.alarm_time.hours; 
    *minutes = Clock.alarm_time.minutes; 
    *seconds = Clock.alarm_time.seconds;
        
    /* release clock variables */ 
    sem_post(&Clock.mutex);
}


/* time_from_alarm_message: extract time from alarm message from user interface */ 
void time_from_alarm_message(char message[], int *hours, int *minutes, int *seconds)
{
    sscanf(message,"alarm:%d:%d:%d",hours, minutes, seconds); 
}


//............................Threads...................

void *clock_thread(void *unused)
{
	// local copies of the current time
	int hours, minutes, seconds;
	
	// inf loop
	while(1)
	{
		// read and display current time
		get_time(&hours, &minutes, &seconds);
		display_time(hours, minutes, seconds);		
		
		if(compare_to_curr_time(hours, minutes, seconds))
		{
			sem_post(&Clock.start_alarm);
		}
		
		// increment time
		increment_time();		
		
		// wait one second
		usleep(1000000);
		
	}
}

int alarm_enabled(void)
{
	int temp;
	
	sem_wait(&Clock.mutex);	
	temp = Clock.alarm_enabled;
	sem_post(&Clock.mutex);
	
	return temp;
	
}

void *alarm_thread(void *unused)
{
	int hours, minutes, seconds;
	
	
	//inf loop
	while(1)
	{
		sem_wait(&Clock.start_alarm);
		
		while(alarm_enabled())
		{	
				display_alarm_text();
				usleep(1000000/2);
				erase_alarm_text();
				usleep(1500000);
						
		}
		
		// Reset the local variable when alarm is disabled
		
	}
}

void *com_thread(void *unused)
{
	 // message array 
    char message[SI_UI_MAX_MESSAGE_SIZE]; 

    // time read from user interface 
    int hours, minutes, seconds; 

    // set GUI size  
    si_ui_set_size(400, 200); 

    while(1)
    {
        // read a message 
        si_ui_receive(message); 
        // check if it is a set message 
        if (strncmp(message, "set", 3) == 0)
        {
            time_from_set_message(message, &hours, &minutes, &seconds); 
            if (time_ok(hours, minutes, seconds))
            {
                clock_set_time(hours, minutes, seconds); 
            }
            else
            {
                si_ui_show_error("Illegal value for hours, minutes or seconds"); 
            }
        }
		// check if it is an set alarm message
		else if (strncmp(message, "alarm", 3) == 0)
		{
			time_from_alarm_message(message, &hours, &minutes,&seconds);
			if (time_ok(hours, minutes, seconds))
            {
                alarm_set_time(hours, minutes, seconds);
            }
            else
            {
                si_ui_show_error("Illegal value for hours, minutes or seconds"); 
            }
		}
		else if (strcmp(message, "stop") == 0)
		{
			// Acknowledge active alarm
			sem_wait(&Clock.mutex);
			Clock.alarm_enabled = 0;
			sem_post(&Clock.mutex);
			
			erase_alarm_time();
		}
        // check if it is an exit message 
        else if (strcmp(message, "exit") == 0)
        {
            exit(0);  	
        }
        // not a legal message
        else 
        {
            si_ui_show_error("unexpected message type"); 
        }
    }
}


//.............................Main.........................

int main(void)
{
	si_ui_init(); 
	
	// initialise display communication
	display_init();
	
	
	
	// initialise variables
	clock_init();
	
	
	pthread_create(&clock_thread_handle, NULL, clock_thread, 0);
	pthread_create(&com_thread_handle, NULL, com_thread, 0);
	pthread_create(&alarm_thread_handle, NULL, alarm_thread, 0);
	
	pthread_join(clock_thread_handle, NULL);
	pthread_join(com_thread_handle, NULL);
	pthread_join(alarm_thread_handle, NULL);
	
	sem_destroy(&Clock.mutex);
	sem_destroy(&Clock.start_alarm);
	
	
	// will never be here!
	return 0;
}
